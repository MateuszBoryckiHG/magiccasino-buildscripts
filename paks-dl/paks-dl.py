#!/usr/bin/env python3
import os
import glob
import pathlib
import shutil
import multiprocessing
import re
from pak_utils import pakTool, pakFolder, paksFromSubfolders, listFiles, globFiles, onPakCreated, callCommand, md5

finalPakList = []
excludeGfx = [".db", ".png", ".csr", ".ivf", ".xls", ".anme", ".pyro"]

def _convertFormatName(fmt):
    return {'dds':'dds',
            'etc':'etc1',
            'etc+':'etc1',
            'etc2':'etc2',
            'etc2+':'etc2',
            'pvr': 'pvr',
            'pvr+': 'pvr'}.get(fmt, None)

def createGraphicPaks(basepath, addXML=False, pathFilter=None):
    pattern = basepath + "*mip"
    atlasFolders = glob.glob(pattern)

    for folder in atlasFolders:
        if pathFilter and not pathFilter(folder):
            continue

        files = os.listdir(folder)
        basename = os.path.basename(folder)
        timestamp = os.path.getmtime(folder + "/" + files[0])

        pak, textureFormat, _ = basename.split(".")
        xml = basepath + pak + ".xml" if addXML else None
        textureFormat = _convertFormatName(textureFormat)
        if not textureFormat:
            continue

        def _createGraphicPak(mipmapLevel, condition, add=None):
            output = "../paks-dl/%s_%d_%s.pak" % (pak, mipmapLevel, textureFormat)

            # Since all mipmap levels are created together,
            # testing one timestamp should suffice
            try:
                if os.path.getmtime(output) > timestamp:
                    print(output, "is up to date (script)")
                    finalPakList.append(output)
                    return
            except:
                pass

            paks = [folder+"/"+p for p in files if condition(p)]
            if add:
                paks.append(add)
            pakTool(output, paks, stripPath=2)

        _createGraphicPak(2, lambda p: not re.match(r'[AC]?[01]$', p), xml)
        _createGraphicPak(1, lambda p: re.match(r'[AC]?1$', p))
        _createGraphicPak(0, lambda p: re.match(r'[AC]?0$', p))


def pakGameData(path, pakPrefix, excludedFolders=[]):
    path = pathlib.Path(path)
    for directory in path.iterdir():
        if not directory.is_dir() or directory.stem in excludedFolders:
            continue
        pakName = pakPrefix + directory.stem + ".pak"
        print(pakName)

        pakFolder("../paks-dl/" + pakName,
                  directory.as_posix() + "/",
                  stripPath=2,
                  excludedExtensions=excludeGfx)

def pakSfx(sfxFolder):
    sfxExtensions = [".rtac", ".ogg"]

    # Localized sfx paking
    rootPath = pathlib.Path("../res-i18n")
    for directory in rootPath.iterdir():
        if not directory.is_dir():
            continue
        soundDir = directory / sfxFolder
        if not soundDir.exists() or not soundDir.is_dir():
            continue

        paksFromSubfolders(soundDir,
                           stripPathFromPakName=rootPath,
                           stripPath=3,
                           includedExtensions=sfxExtensions,
                           pakPrefix="../paks-dl/")

    # Sfx paking
    rootPath = pathlib.Path("../res")
    soundDir = rootPath / sfxFolder
    paksFromSubfolders(soundDir,
                       stripPathFromPakName=rootPath,
                       stripPath=2,
                       includedExtensions=sfxExtensions,
                       pakPrefix="../paks-dl/")


def pakVideo():
    videoExtensions = [".ivf"]
    videoPath = pathlib.Path("../res/slots")
    rootPath = pathlib.Path("../res")
    paksFromSubfolders(videoPath,
                       stripPathFromPakName=rootPath,
                       stripPath=2,
                       includedExtensions=videoExtensions,
                       pakPrefix="../paks-dl/video-",
                       skipIfEmpty=True)

def copyFbPlates():
    os.makedirs("../fb-plates/", exist_ok=True)
    files = glob.glob("../res/plate/welcome*.png")
    for file in files:
        shutil.copy(file, "../fb-plates/")

def deleteOldPaks():
    def silentRemove(filename):
        try:
            os.remove(filename)
        except OSError:
            print("Error during removal of " + filename)

    createdPaks = set([f for f in finalPakList])

    paks = listFiles("../paks-dl/", maxdepth=1, includedExtensions=[".pak"])
    for pak in paks:
        if pak in createdPaks:
            continue

        silentRemove(pak)

        pakName = os.path.basename(pak)
        silentRemove("../paks-server/%s" % pakName)
        for part in glob.glob("../paks-server/%s.part*" % pakName):
            silentRemove(part)

def createAllPaks():
    os.makedirs("../paks", exist_ok=True)
    os.makedirs("../paks-dl", exist_ok=True)
    os.makedirs("../paks-server", exist_ok=True)

    copyFbPlates()

    # data-latest-override.pak
    pakTool("../paks/data-latest-override.pak", ["latest-override.xml"])

    # save list of all created paks
    onPakCreated(lambda p, f: finalPakList.append(p))

    # data.pak
    pakFolder("../paks-dl/data.pak",
              "../res/",
              stripPath=2,
              excludedExtensions=excludeGfx,
              excludedFolders=["plate", "atlases", "music", "sound"],
              excludedSubfoldersOf=["slots"])

    # splash_tip
    pakTool("../paks-dl/splash_tip.pak", globFiles("../res/i18n/*upgrade*.csv"), stripPath=2)

    # data-[hc,bc].pak
    pakFolder("../paks-dl/data-hc.pak",
              "../res-sku/hc/",
              stripPath=3,
              excludedExtensions=excludeGfx)

    pakFolder("../paks-dl/data-bc.pak",
              "../res-sku/bc/",
              stripPath=3,
              excludedExtensions=excludeGfx)

    # data-slots-effects.pak
    excludeGfxNotVideo = list(excludeGfx)
    excludeGfxNotVideo.remove(".ivf")
    pakFolder("../paks-dl/data-slots-effects.pak",
              "../res/slots/effects/",
              stripPath=2,
              excludedExtensions=excludeGfxNotVideo)

    # data-slots_[slot]
    pakGameData("../res/slots/", pakPrefix="data-slots_", excludedFolders=["bonus", "effects", "common"])
    pakGameData("../res/pachislo/", pakPrefix="data-pachislo_")

    # data-slots
    slotsFiles = []
    slotsFiles.extend(listFiles("../res/slots",
                                maxdepth=1,
                                excludedExtensions=excludeGfx))
    slotsFiles.extend(listFiles("../res/slots/common",
                                maxdepth=1,
                                excludedExtensions=excludeGfx))
    pakTool("../paks-dl/data-slots.pak", slotsFiles, stripPath=2)

    # sounds
    pakSfx("sound")
    # music
    pakSfx("music")

    # video
    pakVideo()

    # atlases
    createGraphicPaks("../res/atlases/", addXML=True)
    createGraphicPaks("../res/plate/", addXML=False, pathFilter=lambda path: "splash_huuuge" not in path)

    # delete 'marks'
    deleteOldPaks()

    # create md5
    with open("../paks-server/md5sum", "w", newline='\n') as f:
        paks = pathlib.Path('.').glob("../paks-dl/*.pak")
        paks = sorted(paks)
        for pak in paks:
            f.write("%s *%s\n" %(md5(pak), os.path.basename(pak)))

    # call paks-dl.mk
    callCommand(["make",
                 "-f", "paks-dl.mk",
                 "-j", str(multiprocessing.cpu_count())])

if __name__ == "__main__":
    createAllPaks()
