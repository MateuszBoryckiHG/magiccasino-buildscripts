import jira
import re
import auth

def test1():
    api = jira.JiraApi("https://jira.game-lion.com/rest/api/2/", auth.jiraLogin, auth.jiraPass)
    myself = api.getMyself()

    def addOrUpdateComment(issue, text):
        resp = api.getIssueComments(issue)
        for comment in resp.comments:
            if comment.author.name == myself.name:
                return api.editComment(issue, comment.id, text)
        api.addComment(issue, text)


    addOrUpdateComment("MAGICAS-20007", "Test")


def parseMergelog(log):
    mergedInReg = re.compile(r'Merged in ([a-zA-Z0-9]*-[0-9]*).*\#([0-9]*)\)')

    for line in iter(log.splitlines()):
        res = mergedInReg.search(line)
        if not res:
            continue

        print(res.group(1), res.group(2))


log = """
Merged in MAGICAS-19456-localization-removal-of-steam-city-ids (pull request #6786)
Merged in MAGICAS-20000-fix-amazon-compile-version (pull request #6865)
Merged in MAGICAS-19764-slots-huuuge-link-text-and (pull request #6866)
Merged in MAGICAS-19608-missing-slots-descriptions-fix (pull request #6808)
Merged in MAGICAS-18379-social-reward-disable-localization (pull request #6793)
Merged in MAGICAS-18022-euru-buffalo-rush-minor-fix (pull request #6863)
Merge branch 'MAGICAS-18379-social-reward-disable-localization' of bitbucket.org:gamelionstudios/magiccasino into MAGICAS-18379-social-reward-disable-localization
Merge branch 'master' of bitbucket.org:gamelionstudios/magiccasino into MAGICAS-18379-social-reward-disable-localization
"""

print(auth.jiraPass)
#parseMergelog(log)


