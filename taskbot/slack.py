import json
import re
import argparse
# For Python 3.0 and later
from urllib.request import urlopen, Request
from urllib.parse import urlencode
from urllib.error import HTTPError

def postMessage(url, content):
    headers = {
            "Content-Type": "application/json"
        }

    data = json.dumps(content).encode('utf8')
    request = Request(url,
                        data = data,
                        headers = headers,
                        method = "POST")

    result = urlopen(request)
    result = result.read().decode()
    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--slackUrl', '-u', type=str, default=None, help='Slack url')
    parser.add_argument('--mergeFile', '-m', type=str, required=True, help='Mergefile')
    parser.add_argument('--repoName', '-r', type=str, default="gamelionstudios/magiccasino", help='Name of url')
    parser.add_argument('--text', '-t', type=str, default="Build completed", help='Build message')

    args = parser.parse_args()
    slackUrl = args.slackUrl
    mergeFile = args.mergeFile

    mergedInReg = re.compile(r'Merged in ([a-zA-Z0-9]*-[0-9]*)(.*) \(pull request \#([0-9]*)\)')

    with open(mergeFile) as f:
        content = f.readlines()

    attachment = ""

    for line in content:
        res = mergedInReg.search(line)
        if not res:
            continue
        jiraUrl = "https://jira.game-lion.com/browse/" + res.group(1)
        prUrl = "https://bitbucket.org/" + args.repoName + "/pull-requests/" + res.group(3)
        line = "<%s|%s>%s (<%s|PR %s>)\n" % (jiraUrl, res.group(1), res.group(2), prUrl, res.group(3))
        attachment += line

    message = {}
    message['text'] = args.text
    message['attachments'] = [{
        "text": attachment,
        "mrkdwn_in": ["text"]
    }]

    if slackUrl:
        postMessage(slackUrl, message)
    else:
        print(json.dumps(message))


