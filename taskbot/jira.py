import base64
import json
from collections import namedtuple

# For Python 3.0 and later
from urllib.request import urlopen, Request
from urllib.parse import urlencode
from urllib.error import HTTPError


class JiraApi:
    def __init__(self, baseUrl, username, password):
        self.baseUrl = baseUrl
        auth = '%s:%s' % (username, password)
        try:
            self.auth = base64.b64encode(bytes(auth, "ascii")).decode("ascii") 
        except:
            self.auth = base64.b64encode(auth).decode("ascii") 
        
    def request(self, url, jsonData = None, method = None):
        headers = {
                "Authorization": "Basic %s" % self.auth,
                "Content-Type": "application/json"
            }

        data = json.dumps(jsonData).encode('utf8') if jsonData else None
        url = self.baseUrl + url
        request = Request(url,
                          data = data,
                          headers = headers,
                          method = method)

        result = urlopen(request)
        result = result.read().decode()
        def _json_object_hook(d):
            try:
                return namedtuple('X', d.keys())(*d.values())
            except:
                return d
        return json.loads(result, object_hook=_json_object_hook)

    def getMyself(self):
        url = "myself"
        return self.request(url)

    def getIssue(self, issue):
        url = "issue/%s" % issue
        return self.request(url)

    def getIssueComments(self, issue):
        url = "issue/%s/comment" % issue
        return self.request(url)
    
    def addComment(self, issue, text):
        url = "issue/%s/comment" % issue
        return self.request(url, { 'body': text })
        
    def editComment(self, issue, commentID, text):
        url = "issue/%s/comment/%s" % (issue, commentID)
        return self.request(url, { 'body': text }, method = "PUT")



